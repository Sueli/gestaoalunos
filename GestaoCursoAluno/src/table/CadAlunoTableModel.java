package table;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import model.CadAluno;

public class CadAlunoTableModel extends AbstractTableModel{
    
    public static final int COL_ID = 0;
    public static final int COL_NOME = 1;
    public ArrayList<CadAluno> lista;
    
    public CadAlunoTableModel(ArrayList<CadAluno>l){
        lista = new ArrayList<>(l);
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {  
        return 2;
    }

    @Override
    public Object getValueAt(int linhas, int colunas) {
        CadAluno aluno = lista.get(linhas);
        if (colunas == COL_ID) return aluno.Id;
        if (colunas == COL_NOME) return aluno.Nome;
        return "";
    }
    
    @Override
    public String getColumnName(int colunas) {
        if (colunas == COL_ID) return "Código";
        if (colunas == COL_NOME) return "Nome";
        return "";
    }
}
