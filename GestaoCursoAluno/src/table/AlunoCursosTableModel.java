package table;

import model.AlunoCursos;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class AlunoCursosTableModel extends AbstractTableModel{
    
    public static final int COL_ID = 0;
    //public static final int COL_ID_ALUNO = 1;
    //public static final int COL_ID_CURSO = 2;
    public static final int COL_NOME_ALUNO = 1;
    public static final int COL_DESC_CURSO = 2;
    public ArrayList<AlunoCursos> lista;
    
    public AlunoCursosTableModel(ArrayList<AlunoCursos>l){
        lista = new ArrayList<>(l);
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {  
        return 3;
    }

    @Override
    public Object getValueAt(int linhas, int colunas) {
        AlunoCursos aluno = lista.get(linhas);
        if (colunas == COL_ID) return aluno.id;
        //if (colunas == COL_ID_ALUNO) return aluno.id_Aluno;
        //if (colunas == COL_ID_CURSO) return aluno.id_Curso;
        if (colunas == COL_NOME_ALUNO) return aluno.NomeAluno;
        if (colunas == COL_DESC_CURSO) return aluno.DescCurso;              
        return "";
    }
    
    @Override
    public String getColumnName(int colunas) {
        if (colunas == COL_ID) return "Código";
        if (colunas == COL_NOME_ALUNO) return "Aluno";
        if (colunas == COL_DESC_CURSO) return "Curso";
        return "";
    }
}
