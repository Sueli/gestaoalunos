package table;

import model.CadCurso;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class CadCursoTableModel extends AbstractTableModel{
    
    public static final int COL_ID = 0;
    public static final int COL_DESCRICAO = 1;
    public static final int COL_EMENTA = 2;
    public ArrayList<CadCurso> lista;
    
    public CadCursoTableModel(ArrayList<CadCurso>l){
        lista = new ArrayList<>(l);
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {  
        return 3;
    }

    @Override
    public Object getValueAt(int linhas, int colunas) {
        CadCurso curso = lista.get(linhas);
        if (colunas == COL_ID) return curso.Id;
        if (colunas == COL_DESCRICAO) return curso.Descricao;
        if (colunas == COL_EMENTA) return curso.Ementa;
        return "";
    }
    
    @Override
    public String getColumnName(int colunas) {
        if (colunas == COL_ID) return "Código";
        if (colunas == COL_DESCRICAO) return "Descrição";
        if (colunas == COL_EMENTA) return "Ementa";
        return "";
    }
}
