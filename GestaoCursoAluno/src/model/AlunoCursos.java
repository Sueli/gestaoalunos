package model;

import java.sql.Connection;
import java.util.ArrayList;

public class AlunoCursos extends javax.swing.JFrame {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_Aluno() {
        return id_Aluno;
    }

    public void setId_Aluno(int id_Aluno) {
        this.id_Aluno = id_Aluno;
    }

    public String getNomeAluno() {
        return NomeAluno;
    }

    public void setNomeAluno(String NomeAluno) {
        this.NomeAluno = NomeAluno;
    }

    public int getId_Curso() {
        return id_Curso;
    }

    public void setId_Curso(int id_Curso) {
        this.id_Curso = id_Curso;
    }

    public String getDescCurso() {
        return DescCurso;
    }

    public void setDescCurso(String DescCurso) {
        this.DescCurso = DescCurso;
    }

    @Override
    public String toString() {
        return getNomeAluno();
    }
            
    public int id;
    public int id_Aluno;
    public String NomeAluno;
    public int id_Curso;
    public String DescCurso;
}
