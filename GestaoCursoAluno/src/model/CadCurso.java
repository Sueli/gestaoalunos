package model;

import java.sql.Connection;
import java.util.ArrayList;

public class CadCurso extends javax.swing.JFrame {

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public String getEmenta() {
        return Ementa;
    }

    public void setEmenta(String Ementa) {
        this.Ementa = Ementa;
    }
    
    public int Id;
    public String Descricao;
    public String Ementa;
}
