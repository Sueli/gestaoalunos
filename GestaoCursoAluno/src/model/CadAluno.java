package model;

import java.sql.Connection;
import java.util.ArrayList;

public class CadAluno extends javax.swing.JFrame {

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public int Id;
    public String Nome;
}
