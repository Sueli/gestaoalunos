package view;

import dao.CadAlunoDao;
import javax.swing.JOptionPane;
import model.CadAluno;
import table.CadAlunoTableModel;

public class CadAlunoView extends javax.swing.JFrame {

    CadAluno ac = new CadAluno();
    CadAlunoDao cadao = new CadAlunoDao();
    
    public CadAlunoView() {
        initComponents();
        setLocationRelativeTo(null);
        AlunoTb.setModel(new CadAlunoTableModel(new CadAlunoDao().listarTodos()));
        IdTf.setVisible(false);
        ExcluirBt.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        NomeAlunoTF = new javax.swing.JTextField();
        LimparBt = new javax.swing.JButton();
        ExcluirBt = new javax.swing.JButton();
        SalvarBt = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        AlunoTb = new javax.swing.JTable();
        IdTf = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alunos");

        jLabel1.setText("Nome");

        LimparBt.setText("Limpar");
        LimparBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimparBtActionPerformed(evt);
            }
        });

        ExcluirBt.setText("Excluir");
        ExcluirBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExcluirBtActionPerformed(evt);
            }
        });

        SalvarBt.setText("Salvar");
        SalvarBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarBtActionPerformed(evt);
            }
        });

        AlunoTb.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        AlunoTb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AlunoTbMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(AlunoTb);

        IdTf.setEditable(false);
        IdTf.setText("Código");
        IdTf.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(LimparBt)
                        .addGap(130, 130, 130)
                        .addComponent(ExcluirBt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SalvarBt))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(IdTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(NomeAlunoTF, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 1, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NomeAlunoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IdTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SalvarBt)
                    .addComponent(ExcluirBt)
                    .addComponent(LimparBt))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SalvarBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarBtActionPerformed
        if (NomeAlunoTF.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe um nome para salvar", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else {             
              int novo = AlunoTb. getSelectedRow();
        if (novo < 0){    
           ac.setNome(NomeAlunoTF.getText());
           cadao.inserir(ac);
        } else {
               ac.setId((int) AlunoTb.getValueAt(AlunoTb.getSelectedRow(), CadAlunoTableModel.COL_ID));
               ac.setNome(NomeAlunoTF.getText());
               cadao.alterar(ac);
        }
        }
        AlunoTb.setModel(new CadAlunoTableModel(new CadAlunoDao().listarTodos()));
        NomeAlunoTF.setText("");
        ExcluirBt.setEnabled(false);
    }//GEN-LAST:event_SalvarBtActionPerformed

    private void AlunoTbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlunoTbMouseClicked
        NomeAlunoTF.setText(AlunoTb.getValueAt(AlunoTb.getSelectedRow(), CadAlunoTableModel.COL_NOME).toString());
        ExcluirBt.setEnabled(true);
    }//GEN-LAST:event_AlunoTbMouseClicked

    private void LimparBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimparBtActionPerformed
        AlunoTb.setModel(new CadAlunoTableModel(new CadAlunoDao().listarTodos()));
        NomeAlunoTF.setText("");
    }//GEN-LAST:event_LimparBtActionPerformed

    private void ExcluirBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExcluirBtActionPerformed
        int excluir = JOptionPane.showConfirmDialog(null, "Deseja excluir?", "Excluir Registro", JOptionPane.YES_NO_OPTION);
        if (excluir == 0){
          cadao.excluir((int) AlunoTb.getValueAt(AlunoTb.getSelectedRow(), CadAlunoTableModel.COL_ID));        
          AlunoTb.setModel(new CadAlunoTableModel(new CadAlunoDao().listarTodos()));
          NomeAlunoTF.setText(""); 
        } 
        ExcluirBt.setEnabled(false);
    }//GEN-LAST:event_ExcluirBtActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AlunoCursosView().setVisible(true);               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable AlunoTb;
    private javax.swing.JButton ExcluirBt;
    private javax.swing.JTextField IdTf;
    private javax.swing.JButton LimparBt;
    private javax.swing.JTextField NomeAlunoTF;
    private javax.swing.JButton SalvarBt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
