package view;

import dao.CadCursoDao;
import javax.swing.JOptionPane;
import model.CadCurso;
import table.CadCursoTableModel;

public class CadCursoView extends javax.swing.JFrame {

    CadCurso cc = new CadCurso();
    CadCursoDao acdao = new CadCursoDao();
    
    public CadCursoView() {
        initComponents();
        setLocationRelativeTo(null);
        CursoTb.setModel(new CadCursoTableModel(new CadCursoDao().listarTodos()));
        IdTf.setVisible(false);
        ExcluirBt.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        DescricaoTF = new javax.swing.JTextField();
        EmentaTF = new javax.swing.JTextField();
        LimparBt = new javax.swing.JButton();
        ExcluirBt = new javax.swing.JButton();
        SalvarBt = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        CursoTb = new javax.swing.JTable();
        IdTf = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cursos");

        jLabel1.setText("Descrição");

        jLabel2.setText("Ementa");

        LimparBt.setText("Limpar");
        LimparBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimparBtActionPerformed(evt);
            }
        });

        ExcluirBt.setText("Excluir");
        ExcluirBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExcluirBtActionPerformed(evt);
            }
        });

        SalvarBt.setText("Salvar");
        SalvarBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarBtActionPerformed(evt);
            }
        });

        CursoTb.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        CursoTb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CursoTbMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(CursoTb);

        IdTf.setEditable(false);
        IdTf.setText("Código");
        IdTf.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(LimparBt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
                        .addComponent(ExcluirBt)
                        .addGap(128, 128, 128)
                        .addComponent(SalvarBt))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(31, 31, 31)
                            .addComponent(IdTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(DescricaoTF, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(EmentaTF, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(DescricaoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(EmentaTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IdTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SalvarBt)
                    .addComponent(ExcluirBt)
                    .addComponent(LimparBt))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("Cursos");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SalvarBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarBtActionPerformed
        if (DescricaoTF.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe um curso para salvar", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else {
        int novo = CursoTb. getSelectedRow();
        if (novo < 0){
            cc.setDescricao(DescricaoTF.getText());
            cc.setEmenta(EmentaTF.getText());
            acdao.inserir(cc);
        } else {
            cc.setId((int) CursoTb.getValueAt(CursoTb.getSelectedRow(), CadCursoTableModel.COL_ID));
            cc.setDescricao(DescricaoTF.getText());
            cc.setEmenta(EmentaTF.getText());
            acdao.alterar(cc);
        }
        }
        CursoTb.setModel(new CadCursoTableModel(new CadCursoDao().listarTodos()));
        DescricaoTF.setText("");
        EmentaTF.setText("");
        ExcluirBt.setEnabled(false);
    }//GEN-LAST:event_SalvarBtActionPerformed

    private void CursoTbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CursoTbMouseClicked
        DescricaoTF.setText(CursoTb.getValueAt(CursoTb.getSelectedRow(), CadCursoTableModel.COL_DESCRICAO).toString());
        EmentaTF.setText(CursoTb.getValueAt(CursoTb.getSelectedRow(), CadCursoTableModel.COL_EMENTA).toString());
        ExcluirBt.setEnabled(true);
    }//GEN-LAST:event_CursoTbMouseClicked

    private void LimparBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimparBtActionPerformed
        CursoTb.setModel(new CadCursoTableModel(new CadCursoDao().listarTodos()));
        DescricaoTF.setText("");
        EmentaTF.setText("");
    }//GEN-LAST:event_LimparBtActionPerformed

    private void ExcluirBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExcluirBtActionPerformed
        int excluir = JOptionPane.showConfirmDialog(null, "Deseja excluir?", "Excluir Registro", JOptionPane.YES_NO_OPTION);
        if (excluir == 0){
           acdao.excluir((int) CursoTb.getValueAt(CursoTb.getSelectedRow(), CadCursoTableModel.COL_ID));        
           CursoTb.setModel(new CadCursoTableModel(new CadCursoDao().listarTodos()));
           DescricaoTF.setText("");
           EmentaTF.setText("");              
        }  
        ExcluirBt.setEnabled(false);
    }//GEN-LAST:event_ExcluirBtActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlunoCursosView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AlunoCursosView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable CursoTb;
    private javax.swing.JTextField DescricaoTF;
    private javax.swing.JTextField EmentaTF;
    private javax.swing.JButton ExcluirBt;
    private javax.swing.JTextField IdTf;
    private javax.swing.JButton LimparBt;
    private javax.swing.JButton SalvarBt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
