package dao;

import model.CadAluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CadAlunoDao {
    
    private Connection conn;
    private PreparedStatement stmt;
    private Statement st;
    private ResultSet rs;
    private ArrayList<CadAluno> lista = new ArrayList<>();
    
    public CadAlunoDao(){
        conn = new ConnectionFactory().getConnection();
    }

    public void inserir(CadAluno aluno){
           String sql = "insert into Aluno (nome) values (?);";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setString(1, aluno.Nome);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao inserir: "+erro);
           }     
    }
    
    public void alterar(CadAluno aluno){
           String sql = "update Aluno set nome = ? where id = ?";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setString(1, aluno.Nome);
              stmt.setInt(2, aluno.Id);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao alterar: "+erro);
           }     
    }
    
    public void excluir(int id){
           String sql = "delete from Aluno where id = "+id;
           try{
              st = conn.createStatement();
              st.execute(sql);
              st.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao exclur: "+erro);
           }     
    }
    
    public ArrayList<CadAluno> listarTodos(){
        String sql = "select * from Aluno";
        try{
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()){
                CadAluno aluno = new CadAluno();
                aluno.setId(rs.getInt("id"));
                aluno.setNome(rs.getString("Nome"));
                lista.add(aluno);
            }
        }catch(Exception erro){
            throw new RuntimeException();
        }
        
        return lista;
    }
       
}
