package dao;

import model.CadCurso;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CadCursoDao {
    
    private Connection conn;
    private PreparedStatement stmt;
    private Statement st;
    private ResultSet rs;
    private ArrayList<CadCurso> lista = new ArrayList<>();
    
    public CadCursoDao(){
        conn = new ConnectionFactory().getConnection();
    }

    public void inserir(CadCurso curso){
           String sql = "insert into Curso (descricao, ementa) values (? , ?);";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setString(1, curso.Descricao);
              stmt.setString(2, curso.Ementa);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao inserir: "+erro);
           }     
    }
    
    public void alterar(CadCurso curso){
           String sql = "update curso set descricao = ?, ementa = ? where id = ?";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setString(1, curso.Descricao);
              stmt.setString(2, curso.Ementa);
              stmt.setInt(3, curso.Id);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao alterar: "+erro);
           }     
    }
    
    public void excluir(int id){
           String sql = "delete from curso where id = "+id;
           try{
              st = conn.createStatement();
              st.execute(sql);
              st.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao exclur: "+erro);
           }     
    }
    
    public ArrayList<CadCurso> listarTodos(){
        String sql = "select * from curso";
        try{
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()){
                CadCurso curso = new CadCurso();
                curso.setId(rs.getInt("Id"));
                curso.setDescricao(rs.getString("descricao"));
                curso.setEmenta(rs.getString("ementa"));
                lista.add(curso);
            }
        }catch(Exception erro){
            throw new RuntimeException();
        }
        
        return lista;
    }
}
