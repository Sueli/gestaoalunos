package dao;

import model.AlunoCursos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.CadAluno;
import model.CadCurso;

public class AlunoCursosDao {
    
    private Connection conn;
    private PreparedStatement stmt;
    private Statement st;
    private ResultSet rs;
    private ArrayList<AlunoCursos> lista = new ArrayList<>();
    private ArrayList<CadAluno> lAluno = new ArrayList<>();
    private ArrayList<CadCurso> lCurso = new ArrayList<>();
    
    public AlunoCursosDao(){
        conn = new ConnectionFactory().getConnection();
    }

    public void inserir(AlunoCursos aluno){
           String sql = "insert int curso_Aluno (id_Aluno, id_Curso) values (?,?);";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setInt(1, aluno.id_Aluno);
              stmt.setInt(2, aluno.id_Curso);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao inserir: "+erro);
           }     
    }
    
    public void alterar(AlunoCursos aluno){
           String sql = "update curso_Aluno set id_Aluno = ?, id_Curso = ? where id = ?";
           try{
              stmt = conn.prepareStatement(sql);
              stmt.setInt(1, aluno.id_Aluno);
              stmt.setInt(2, aluno.id_Curso);
              stmt.setInt(3, aluno.id);
              stmt.execute();
              stmt.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao alterar: "+erro);
           }     
    }
    
    public void excluir(int id){
           String sql = "delete from curso_Aluno where id = "+id;
           try{
              st = conn.createStatement();
              st.execute(sql);
              st.close();
           }catch(Exception erro){
               throw new RuntimeException("Erro ao exclur: "+erro);
           }     
    }
    
    public ArrayList<AlunoCursos> listarTodos(){
        String sql = "select ca.id, a.id id_Aluno, a.nome NomeAluno, c.id id_Curso, c.descricao DescCurso"
                +" from curso_Aluno ca"
                +" join aluno a on (a.id = ca.id_aluno)"
                +" join curso c on (c.id = ca.id_curso)";
        try{
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()){
                AlunoCursos aluno = new AlunoCursos();
                aluno.setId(rs.getInt("id"));
                aluno.setId_Aluno(rs.getInt("id_Aluno"));
                aluno.setNomeAluno(rs.getString("NomeAluno"));
                aluno.setId_Curso(rs.getInt("id_Curso"));
                aluno.setDescCurso(rs.getString("DescCurso"));
                lista.add(aluno);
            }
        }catch(Exception erro){
            throw new RuntimeException();
        }
        
        return lista;
    }
    
    public ArrayList<CadAluno> listarAlunos(){
        String sql = "select a.*"
                +" from Aluno a";
        try{
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()){
                CadAluno aluno = new CadAluno();
                aluno.setNome(rs.getString("Nome"));
                lAluno.add(aluno);
            }
        }catch(Exception erro){
            throw new RuntimeException();
        }
        
        return lAluno;
    } 
    
    public ArrayList<CadCurso> listarCursos(){
        String sql = "select c.*"
                +" from Curso c";
        try{
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()){
                CadCurso curso = new CadCurso();
                curso.setDescricao(rs.getString("Descricao"));
                lCurso.add(curso);
            }
        }catch(Exception erro){
            throw new RuntimeException();
        }
        
        return lCurso;
    }
    
}
