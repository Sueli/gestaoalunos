create table curso (
Id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
Descricao VARCHAR(50),
Ementa TEXT);

create table aluno (
Id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
Nome VARCHAR(50));

create table curso_aluno (
Id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
Id_Aluno SMALLINT UNSIGNED NOT NULL,
Id_Curso SMALLINT UNSIGNED NOT NULL);

alter table curso_aluno add
constraint FK_Curso foreign key (Id_Curso)
references curso(Id);

alter table curso_aluno add
constraint FK_Aluno foreign key (Id_Aluno)
references Aluno(Id);
